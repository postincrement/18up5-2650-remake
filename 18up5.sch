EESchema Schematic File Version 2
LIBS:2650
LIBS:power
LIBS:device
LIBS:switches
LIBS:relays
LIBS:motors
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:29ee010
LIBS:18up5-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 3
Title "18up5 2650 Minicomputer"
Date "2018-03-18"
Rev "1"
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	11050 5950 11050 2350
$Sheet
S 750  750  1700 1050
U 5889A917
F0 "CPU, ROM and Interface" 60
F1 "main.sch" 60
F2 "D[0..7]" I R 2450 950 50 
F3 "AD[0..9]" I R 2450 1100 50 
$EndSheet
$Sheet
S 2800 750  1800 1050
U 5889CFB3
F0 "RAM and Pins" 60
F1 "rom_ram.sch" 60
F2 "D[0..7]" I L 2800 950 50 
F3 "AD[0..9]" I L 2800 1100 50 
$EndSheet
Wire Bus Line
	2800 950  2450 950 
Wire Bus Line
	2450 1100 2800 1100
$EndSCHEMATC
