# A remake of the EA 2650 mini system

*By Craig Southeren, craigs@postincrement.com*

In May 1978, Electronics Australia described a small development board based on the Signetics 2650 microprocessor. For many people in Australia (the author included), this was the first computer that was cheap enough to buy and build even if you were a student or
beginner. 

This update to the simple and elegant design of the original provides a new PCB that evokes the original while adding modern alternatives to some of the hardto- get parts of the original.

![18up5](http://www.postincrement.net/wp-content/uploads/2018/03/populated-board-1024x434.png "18up5")

This repo contains the full Kicad schematics and board layouts for this project