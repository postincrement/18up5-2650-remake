EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:2650
LIBS:switches
LIBS:17up5-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 4
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L 74LS123 U5
U 1 1 5A71BFF5
P 2900 2650
F 0 "U5" H 2900 2600 50  0000 C CNN
F 1 "74LS123" H 2900 2500 50  0000 C CNN
F 2 "Housings_DIP:DIP-16_W7.62mm_LongPads" H 2900 2650 50  0001 C CNN
F 3 "" H 2900 2650 50  0001 C CNN
	1    2900 2650
	1    0    0    -1  
$EndComp
$Comp
L 74LS123 U5
U 2 1 5A71C037
P 4800 2650
F 0 "U5" H 4800 2600 50  0000 C CNN
F 1 "74LS123" H 4800 2500 50  0000 C CNN
F 2 "" H 4800 2650 50  0001 C CNN
F 3 "" H 4800 2650 50  0001 C CNN
	2    4800 2650
	1    0    0    -1  
$EndComp
Text GLabel 6100 2350 2    60   Output ~ 0
CLOCK
$Comp
L Oscillator Y1
U 1 1 5A7516D8
P 3100 4150
F 0 "Y1" H 3100 4350 50  0000 C CNN
F 1 "Oscillator" H 3250 3950 50  0000 C CNN
F 2 "Housings_DIP:DIP-8_W7.62mm_LongPads" H 3100 4150 50  0001 C CNN
F 3 "" H 3100 4150 50  0001 C CNN
	1    3100 4150
	1    0    0    -1  
$EndComp
Text GLabel 2200 4150 0    60   Output ~ 0
CLOCK
$Comp
L GND #PWR042
U 1 1 5A751BC1
P 3000 4700
F 0 "#PWR042" H 3000 4450 50  0001 C CNN
F 1 "GND" H 3000 4550 50  0000 C CNN
F 2 "" H 3000 4700 50  0001 C CNN
F 3 "" H 3000 4700 50  0001 C CNN
	1    3000 4700
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR043
U 1 1 5A751BEE
P 2950 3800
F 0 "#PWR043" H 2950 3650 50  0001 C CNN
F 1 "+5V" H 2950 3940 50  0000 C CNN
F 2 "" H 2950 3800 50  0001 C CNN
F 3 "" H 2950 3800 50  0001 C CNN
	1    2950 3800
	1    0    0    -1  
$EndComp
$Comp
L C_Small C7
U 1 1 5A751EA3
P 3100 1700
F 0 "C7" H 3110 1770 50  0000 L CNN
F 1 "270p" H 3110 1620 50  0000 L CNN
F 2 "Capacitors_THT:C_Disc_D3.8mm_W2.6mm_P2.50mm" H 3100 1700 50  0001 C CNN
F 3 "" H 3100 1700 50  0001 C CNN
	1    3100 1700
	1    0    0    -1  
$EndComp
$Comp
L R_Small R6
U 1 1 5A751F0D
P 2700 1350
F 0 "R6" H 2730 1370 50  0000 L CNN
F 1 "6k8" H 2730 1310 50  0000 L CNN
F 2 "Resistors_THT:R_Axial_DIN0411_L9.9mm_D3.6mm_P12.70mm_Horizontal" H 2700 1350 50  0001 C CNN
F 3 "" H 2700 1350 50  0001 C CNN
	1    2700 1350
	1    0    0    -1  
$EndComp
$Comp
L R_Small R7
U 1 1 5A751F40
P 4600 1350
F 0 "R7" H 4630 1370 50  0000 L CNN
F 1 "22k" H 4630 1310 50  0000 L CNN
F 2 "Resistors_THT:R_Axial_DIN0411_L9.9mm_D3.6mm_P12.70mm_Horizontal" H 4600 1350 50  0001 C CNN
F 3 "" H 4600 1350 50  0001 C CNN
	1    4600 1350
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR044
U 1 1 5A75203E
P 1800 2400
F 0 "#PWR044" H 1800 2150 50  0001 C CNN
F 1 "GND" H 1800 2250 50  0000 C CNN
F 2 "" H 1800 2400 50  0001 C CNN
F 3 "" H 1800 2400 50  0001 C CNN
	1    1800 2400
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR045
U 1 1 5A752080
P 3950 2550
F 0 "#PWR045" H 3950 2300 50  0001 C CNN
F 1 "GND" H 3950 2400 50  0000 C CNN
F 2 "" H 3950 2550 50  0001 C CNN
F 3 "" H 3950 2550 50  0001 C CNN
	1    3950 2550
	1    0    0    -1  
$EndComp
$Comp
L R_Small R8
U 1 1 5A7520F7
P 5750 1450
F 0 "R8" H 5780 1470 50  0000 L CNN
F 1 "2k2" H 5780 1410 50  0000 L CNN
F 2 "Resistors_THT:R_Axial_DIN0411_L9.9mm_D3.6mm_P12.70mm_Horizontal" H 5750 1450 50  0001 C CNN
F 3 "" H 5750 1450 50  0001 C CNN
	1    5750 1450
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR046
U 1 1 5A75214C
P 5750 800
F 0 "#PWR046" H 5750 650 50  0001 C CNN
F 1 "+5V" H 5750 940 50  0000 C CNN
F 2 "" H 5750 800 50  0001 C CNN
F 3 "" H 5750 800 50  0001 C CNN
	1    5750 800 
	1    0    0    -1  
$EndComp
$Comp
L POT RV1
U 1 1 5A7521EC
P 2950 1100
F 0 "RV1" V 2775 1100 50  0000 C CNN
F 1 "10k" V 2850 1100 50  0000 C CNN
F 2 "Potentiometers:Potentiometer_WirePads_largePads" H 2950 1100 50  0001 C CNN
F 3 "" H 2950 1100 50  0001 C CNN
	1    2950 1100
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5550 2350 6100 2350
Wire Wire Line
	2200 4150 2600 4150
Wire Wire Line
	3000 4700 3000 4500
Wire Wire Line
	2950 3900 2950 3800
Wire Wire Line
	5000 1850 5000 1800
Connection ~ 4600 1550
Wire Wire Line
	3100 1850 3100 1800
Wire Wire Line
	2700 1450 2700 1850
Wire Wire Line
	3700 2950 3700 3600
Wire Wire Line
	3700 3600 2050 3600
Wire Wire Line
	2050 3600 2050 2450
Wire Wire Line
	2050 2450 2150 2450
Wire Wire Line
	3650 2950 3700 2950
Wire Wire Line
	3650 2350 3850 2350
Wire Wire Line
	3850 2350 3850 2450
Wire Wire Line
	3850 2450 4050 2450
Wire Wire Line
	2150 2300 1800 2300
Wire Wire Line
	1800 2300 1800 2400
Wire Wire Line
	4050 2300 3950 2300
Wire Wire Line
	3950 2300 3950 2550
Wire Wire Line
	5750 3450 2900 3450
Wire Wire Line
	5750 800  5750 1350
Wire Wire Line
	5750 1550 5750 3450
Connection ~ 4800 3450
Wire Wire Line
	2700 950  2700 1250
Wire Wire Line
	2700 1100 2800 1100
Wire Wire Line
	2950 950  2700 950 
Connection ~ 2700 1100
Wire Wire Line
	3100 1100 5750 1100
Wire Wire Line
	4600 1250 4600 1100
Connection ~ 4600 1100
Connection ~ 5750 1100
Wire Wire Line
	3100 1550 2700 1550
Connection ~ 2700 1550
Wire Wire Line
	4600 1450 4600 1850
Text GLabel 6100 1750 2    60   Output ~ 0
PULLUP
Wire Wire Line
	6100 1750 5750 1750
Connection ~ 5750 1750
$Comp
L C_Small C8
U 1 1 5A751EE6
P 5000 1700
F 0 "C8" H 5010 1770 50  0000 L CNN
F 1 "47p" H 5010 1620 50  0000 L CNN
F 2 "Capacitors_THT:C_Disc_D4.3mm_W1.9mm_P5.00mm" H 5000 1700 50  0001 C CNN
F 3 "" H 5000 1700 50  0001 C CNN
	1    5000 1700
	1    0    0    -1  
$EndComp
Wire Wire Line
	5000 1600 5000 1550
Wire Wire Line
	5000 1550 4600 1550
Wire Wire Line
	3100 1550 3100 1600
NoConn ~ 5550 2950
NoConn ~ 3450 4100
Wire Wire Line
	3300 4100 3450 4100
$EndSCHEMATC
